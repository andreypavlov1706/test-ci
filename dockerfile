FROM node:10-alpine as build-stage

ARG BUILD_ENV

WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY . /app/
RUN npm run build $BUILD_ENV
#RUN npm run build
RUN ls /app/dist/gitlab-ci

FROM nginx
COPY --from=build-stage /app/dist/gitlab-ci /usr/share/nginx/html
COPY --from=build-stage /app/.deploy/nginx/nginx.conf /etc/nginx/conf.d/default.conf
